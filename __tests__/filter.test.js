import { filterEvenNumbers, filterLengthWith4, filterStartWithA } from '../src/filter';

describe('filter test', () => {
    describe('filterEvenNumbers', () => {
        // Please add test cases here
        test("should return the even number", () => {
            //GIVEN
            const numbers = [2, 4, 5];
    
            //WHEN
            const result = filterEvenNumbers(numbers);
    
            //THEN
            expect(result).toEqual([2, 4]);
        });
    
    });
    
    describe('filterLengthWith4', () => {
        // Please add test cases here
        test("should return the words with the length of 4", () => {
            //GIVEN
            const words = ["four", "apple", "mango"];
    
            //WHEN
            const result = filterLengthWith4(words);
    
            //THEN
            expect(result).toEqual(["four"]);
        });
    });
    
    describe('filterStartWithA', () => {
        // Please add test cases here
        test("should return the words starting with a", () => {
            //GIVEN
            const words = ["apple", "mango"];
    
            //WHEN
            const result = filterStartWithA(words);
    
            //THEN
            expect(result).toEqual(["apple"]);
        });
    
    });
})
