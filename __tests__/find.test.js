import { firstGrownUp, firstOrange, firstLengthOver5Name } from "../src/find";


describe('find test', () => {
  describe('firstGrownUp', () => {
    // Please add test cases here
    test("should return the first number larger than or equal to 18", () => {
        //GIVEN
        const numbers = [17, 48, 18];
  
        //WHEN
        const result = firstGrownUp(numbers);
  
        //THEN
        expect(result).toBe(48);
    });
  
  });
  
  describe('firstOrange', () => {
    // Please add test cases here
    test("should return the first orange", () => {
        //GIVEN
        const words = ["apple", "orange", "mango", "orange"];
  
        //WHEN
        const result = firstOrange(words);
  
        //THEN
        expect(result).toBe("orange");
    });
  });
  
  describe('firstLengthOver5Name', () => {
    // Please add test cases here
    test("should return the first name with the length longer than 5", () => {
        //GIVEN
        const words = ["Joe", "Ken", "Michael", "Sophia"];
  
        //WHEN
        const result = firstLengthOver5Name(words);
  
        //THEN
        expect(result).toBe("Michael");
    });
  
  });
})
