import { addSerialNumber, halfNumbers, spliceNames } from "../src/map";

describe('map test', () => {
  describe('halfNumbers', () => {
    // Please add test cases here
    test("should return the numbers divided by 2", () => {
        //GIVEN
        const numbers = [4, 6, 9, 17];
  
        //WHEN
        const result = halfNumbers(numbers);
  
        //THEN
        expect(result).toEqual([2, 3, 4.5, 8.5]);
    });
  
  });
  
  describe('spliceNames', () => {
    // Please add test cases here
    test("should return the string starting with Hello", () => {
        //GIVEN
        const words = ["Joe", "Michael", "Natalie"];
  
        //WHEN
        const result = spliceNames(words);
  
        //THEN
        expect(result).toEqual(["Hello Joe", "Hello Michael", "Hello Natalie"]);
    });
  });
  
  describe('addSerialNumber', () => {
    // Please add test cases here
    test("should return the string starting with serial number", () => {
        //GIVEN
        const words = ["apple", "mango", "orange"];
  
        //WHEN
        const result = addSerialNumber(words);
  
        //THEN
        expect(result).toEqual(["1. apple", "2. mango", "3. orange"]);
    });
  
  });
})

